package
{
	import ai.NPC;
	import ai.NPCEvent;
	import ai.PseudoNPC;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#FFFFFF")]
	public class ContainerCreator extends Sprite
	{
		public var npc:NPC;
		
		public function ContainerCreator()
		{
			npc = new NPC(1);
			
			addEventListener(Event.ADDED, onAdd);
		}
		
		public function onAdd(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, input);
			stage.addEventListener(MouseEvent.CLICK, think);
		}
		
		private function input(e:Event):void
		{
			// when a choice is selected, dispatch and NPC input event
			npc.dispatchEvent(new NPCEvent(NPCEvent.INPUT, "buy sword"));
		}
		
		private function think(e:Event):void
		{
			npc.think();
		}
	}
}