package ai
{
	import flash.utils.Dictionary;

	public class ChoiceNode extends TreeNode
	{
		public var question:String; // a question that would ask for the player's choice
		public var map:Dictionary = new Dictionary(); // maps choices to corresponding tree node, USE STRING VALUES HERE
		
		public function ChoiceNode(question:String)
		{
			this.question = question;
		}
		
		public function addValue(value:String, node:TreeNode):void
		{
			map[value] = node;
		}
		
		public override function run(owner:*):TreeNode
		{
			var choices:Array = new Array();
			for (var key:Object in map) {
				// iterates through each object key
				choices.push(key);
			}
			// prompt the player,
			owner.prompt(question, choices);
			// then wait for input
			return new WaitingNode(this);
		}
		
		public function runWithInput(input:String, owner:*):TreeNode
		{
			return (map[input] as TreeNode).run(owner);
		}
	}
}