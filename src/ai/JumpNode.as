package ai
{
	import flash.utils.Dictionary;

	public class JumpNode extends TreeNode
	{
		public var jumpID:String;
		public var jumpMap:Dictionary;
		
		public function JumpNode(jumpID:String, jumpMap:Dictionary)
		{
			this.jumpID = jumpID;
			this.jumpMap = jumpMap;
		}
		
		public override function run(owner:*):TreeNode
		{
			return (jumpMap[jumpID] as TreeNode).run(owner);
		}
	}
}