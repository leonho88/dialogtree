package ai
{
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;

	public class NPC extends EventDispatcher
	{	
		public var root:TreeNode;
		public var currNode:TreeNode;
		public var nextNode:TreeNode;
		public var dialogMap:Dictionary = new Dictionary();
		
		public var bb:*;//Blackboard; //pointer to blackboard;
		
		public function NPC(id:int)
		{
			this.id = id;
			this.name = Dialogue.dialog.NPC.(@id == id).@name;
			
			trace(id, name);
			
			createTree();
			
			this.addEventListener(NPCEvent.INPUT, handleInput);
			this.addEventListener(NPCEvent.INTERRUPT, handleInterrupt);
			this.addEventListener(NPCEvent.TIMEOUT, handleTimeout);
			
			// set default properties for an NPC
			this.setProperty("polite", "true");
			this.setProperty("stranger", "true");
			this.setProperty("relationship", 1);
			this.setProperty("questGiven", "false");
			this.setProperty("questDone", "false");
			this.setProperty("questComplete", "false");
		}
		
		public function think():void
		{
			// if no more next node, restart from root
			// else run the next node
			if (!nextNode)
				nextNode = root;
			else 
				nextNode = nextNode.run(this);
		}
		
		public function setProperty(prop:String, val:*):void
		{
			properties[prop] = val;
		}
		
		public function getProperty(prop:String):*
		{
			return properties[prop];
		}
		
		public function incProperty(prop:String, val:Number):void
		{
			properties[prop] += val;
		}
		
		public function decProperty(prop:String, val:Number):void
		{
			properties[prop] -= val;
		}
		
		public function say(msg:String):void
		{
			trace(msg);
			// pass msg to Manager for npc to say it
		}
		
		public function prompt(msg:String, choices:Array):void
		{
			// pass msg and choices to Manager to ask player for choices
			trace(msg + " " + choices);	
		}
		
		// temporary
		public function giveSword(amt:int):void
		{
			trace("Give player: " + amt + " swords.");
		}
		
		public function givePotion(amt:int):void
		{
			trace("Give player: " + amt + " potions.");
		}
		
		public function takeMoney(amt:int):void
		{
			trace("Take " + amt + " gold from player.");
		}
		
		private function handleInterrupt(e:NPCEvent):void
		{
			nextNode = dialogMap[e.type];
			think();
		}
		
		private function handleTimeout(e:NPCEvent):void
		{
			nextNode = dialogMap[e.type];
			think();
		}
		
		private function handleInput(e:NPCEvent):void
		{
			(nextNode as WaitingNode).input = e.input;
			think();
		}
		
		private function createTree():void
		{
			// choose the npc's tree
			var dialogTree:* = Dialogue.dialog.NPC.(@id == id);
			for (var i:int = 0; i < dialogTree.node.length(); i++)
			{
				trace ("ID: " + dialogTree.node[i].@id + "\n" + dialogTree.node[i]);
				dialogMap[String(dialogTree.node[i].@id)] = parseTree(dialogTree.node[i]);
			}
			
			for (var k:Object in dialogMap) {
				var node:TreeNode = dialogMap[k];
				var value:String = String(k);
				trace(value, node);
			}
			
			this.root = dialogMap[String(dialogTree.node[0].@id)];
			this.nextNode = this.root;
		}
		
		private function parseTree(dialogTree:*):TreeNode
		{
			var node:TreeNode = null;
			
			var i:int = 0;
			
			if (dialogTree.@type == "null")
			{
				node = null;
			}
			else if (dialogTree.@type == "condition")
			{
				node =  new SwitchNode(dialogTree.@condition);
				for (i = 0; i < dialogTree.value.length(); i++)
				{
					(node as SwitchNode).addValue(dialogTree.value[i].@value, parseTree(dialogTree.value[i].node));
				}
			}
			else if (dialogTree.@type == "dialog")
			{
				node = new DialogNode(dialogTree.msg);
				node.next = parseTree(dialogTree.next.node);
			}
			else if (dialogTree.@type == "choice")
			{
				node = new ChoiceNode(dialogTree.question);
				for (i = 0; i < dialogTree.value.length(); i++)
				{
					(node as ChoiceNode).addValue(dialogTree.value[i].@value, parseTree(dialogTree.value[i].node));
				}
			}
			else if (dialogTree.@type == "jump")
			{
				node = new JumpNode(dialogTree, dialogMap);
			}
			else if (dialogTree.@type == "command")
			{
				node = new CommandNode();
				for (i = 0; i < dialogTree.cmd.length(); i++)
				{
					(node as CommandNode).addValue(dialogTree.cmd[i]);
				}
				node.next = parseTree(dialogTree.next.node);
			}
			return node;
		}
		
		private var id:int;
		private var name:String;
		private var properties:Dictionary = new Dictionary();
	}
}