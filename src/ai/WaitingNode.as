/*
WaitingNode
a special node used internally by ChoiceNode when the node is waiting for input
author: Leon Ho
*/
package ai
{
	public class WaitingNode extends TreeNode
	{
		public var input:String;
		
		private var waitingNode:ChoiceNode;
		
		public function WaitingNode(choiceNode:ChoiceNode)
		{
			this.waitingNode = choiceNode;
		}
		
		override public function run(owner:*):TreeNode
		{
			return waitingNode.runWithInput(input, owner);
		}
	}
}