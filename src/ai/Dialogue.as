package ai
{
	public class Dialogue
	{
		public static var dialog: XML = <dialogTree>
	<NPC id='1' name='Shopkeeper'>
		<node id="100" type="condition" condition="self polite">
			<value value="true">
				<node type="dialog">
					<msg>Welcome!</msg>
					<next>
						<node type="jump">101</node>
					</next>
				</node>
			</value>
			<value value="false">
				<node type="dialog">
					<msg>What do you want!?</msg>
					<next>
						<node type="jump">101</node>
					</next>
				</node>
			</value>
		</node>
		<node id="101" type="choice">
			<question>Choose something:</question>
			<value value="buy sword">
				<node type="command">
					<cmd>self giveSword 1</cmd>
					<cmd>self takeMoney 100</cmd>
					<next>
						<node type="dialog">
							<msg>Anything else?</msg>
							<next>
								<node type="jump">101</node>
							</next>
						</node>
					</next>
				</node>
			</value>
			<value value="buy potion">
				<node type="command">
					<cmd>self givePotion 1</cmd>
					<cmd>self takeMoney 10</cmd>
					<next>
						<node type="dialog">
							<msg>Anything else?</msg>
							<next>
								<node type="jump">101</node>
							</next>
						</node>
					</next>
				</node>
			</value>
			<value value="nothing">
				<node type="jump">1end</node>
			</value>
		</node>
		
		<node id="1end" type="condition" condition="self polite">
			<value value="true">
				<node type="dialog">
					<msg>Please come again!</msg>
					<next>
						<node type="null"></node>
					</next>
				</node>
			</value>
			<value value="false">
				<node type="dialog">
					<msg>Get lost!</msg>
					<next>
						<node type="null"></node>
					</next>
				</node>
			</value>
		</node>
		
		<node id="timeout" type="dialog">
			<msg>Keep a man waiting, why don't you?</msg>
			<next><node type="null"></node></next>
		</node>
		
	</NPC>

	<NPC id='2' name ='Quest Giver'>	
		<node id="200" type="condition" condition="self stranger">
			<value value="true">
				<node type="dialog">
					<msg>Hi there! I am Quest Giver.</msg>
					<next>
						<node type="command">
							<cmd>self setProperty stranger false</cmd>
							<next>
								<node type="jump">201</node>
							</next>
						</node>
					</next>
				</node>
			</value>
			<value value="false">
				<node type="dialog">
					<msg>How are you?</msg>
					<next>
						<node type="jump">201</node>
					</next>			
				</node>
			</value>
		</node>
		<node id="201" type="condition" condition="self questGiven">
			<value value="false">
				<node type="choice">
					<question>Would you care to help me with something?</question>
					<value value="yes">
						<node type="command">
							<cmd>self setProperty questGiven true</cmd>
							<next>
								<node type="jump">questInfo</node>
							</next>
						</node>
					</value>
					<value value="no">
						<node type="command">
							<!-- relationship decreases -->
							<next>
								<node type="dialog">
									<msg>...Okay</msg>
									<next>
										<node type="null"></node>
									</next>
								</node>
							</next>
						</node>
					</value>
				</node>
			</value>
			<value value="true">
				<node type="condition" condition="self questDone">
					<value value="true">
						<node type="dialog">
							<msg>Thank you for your help. I will let you know if I need your help again.</msg>
							<next>
								<node type="null"></node>
							</next>
						</node>
					</value>
					<value value="false">
						<node type="dialog">
							<msg>How is it going?</msg>
							<next>
								<node type="condition" condition="self questCompleted">
									<value value="true">
										<node type="dialog">
											<msg>Report quest progress</msg>
											<next>
												<node type="dialog">
													<msg>Thank you very much! That really saved me.</msg>
													<next>
														<node type="command">
															<!-- questDone = true -->
															<cmd>self setProperty questDone true</cmd>
															<next>
																<node type="null"></node>
															</next>
														</node>
													</next>
												</node>
											</next>
										</node>
									</value>
									<value value="false">
										<node type="jump">questInfo</node>
									</value>
								</node>
							</next>
						</node>
					</value>
				</node>
			</value>
		</node>	
		<node id="questInfo" type="choice">
			<value value="ask what to do">
				<node type="dialog">
					<msg>You need to help me find the sacred ring.</msg>
					<node type="jump">questInfo</node>
				</node>
			</value>
			<value value="ask where to go">
				<node type="dialog">
					<msg>Go to the caves to the East of the village.</msg>
					<node type="jump">questInfo</node>
				</node>
			</value>
			<value value="leave">
				<node type="null"></node>
			</value>	
		</node>
	</NPC>
</dialogTree>
		
		public function Dialogue()
		{
			
					
			//dialog with questgiver
			//conversation
					var k = dialog.NPC.(@id == 2).chat.c.length();
			
				
			
				
			//dialog with shopkeeper
			//if relationship == good
//				trace(dialog.NPC.(@id == 1).intros.intro.(@id== 'polite'));
//			//else
//				trace(dialog.NPC.(@id == 1).intros.intro.(@id == 'rude'));
				
//			//conversation
//				var k = dialog.NPC.(@id == 1).chat.c.length();
//				for(var j=0; j < k; j++){
//				trace(dialog.NPC.(@id == 1).chat.c[j].text);
//				}
//				var x = 1 //chosen option
//
//				if(dialog.NPC.(@id == 1).chat.c[x].response.nextDialog != 0){
//					for(var j=0; j < k; j++){
//					trace(dialog.NPC.(@id == 1).chat.(@id==dialog.NPC.(@id == 1).chat.c[x].response.nextDialog).c[j].text);
//					}
//				}
			//if relationship == good	
//				trace(dialog.NPC.(@id == 1).goodbyes.goodbye.(@id== 'polite'));
//			//else
//				trace(dialog.NPC.(@id == 1).goodbyes.goodbye.(@id== 'rude'));
			trace("done");
			
		}
	}
}